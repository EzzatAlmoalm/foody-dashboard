import styled from "@emotion/styled";
import {
  Autocomplete,
  Box,
  Button,
  Chip,
  FormControl,
  InputLabel, 
  MenuItem,
  Paper,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import React, { useRef, useState } from "react";
import { categories } from "../data/categories";

import { useParams } from "react-router-dom";
import { products } from "../data/products";

const EditCategory = () => {
  const { id } = useParams();
  const product = products.find((product) => product.id === parseInt(id));

  const { Originalcategory, image, price, instock, product_name, short_description, quantity, } =
    products;
  const [category, setCategory] = useState("");
  console.log(Originalcategory)
 
  const handleChange = (event) => {
    setCategory(event.target.value);
    console.log(category);
  };



  return (
    <Box sx={{ pt: "80px", pb: "20px" }}>
      <Typography variant="h6" sx={{ marginBottom: "14px" }}>
        Edit Product Category
      </Typography>
      <Paper
        sx={{
          boxShadow: "none !important",
          borderRadius: "12px",
          borderStyle: "solid",
          borderWidth: "1px",
          borderColor: "divider",
          p: "20px",
          maxWidth: "800px",
          margin: "0 auto",
          cursor: "pointer",
          overflow: "hidden",
        }}
      >
        <Box sx={{ my: 2 }}>
          <TextField
            label="Product Name"
            variant="outlined"
            size="small"
            fullWidth
            defaultValue={product_name}

          />
        </Box>
       
        <Box sx={{ my: 2 }}>
          <TextField
            label="Product Quantity"
            variant="outlined"
            size="small"
            fullWidth
            defaultValue={quantity}

          />
        </Box>
       
        <Box sx={{ mt: 4 }}>
          <FormControl fullWidth size="small">
            <InputLabel id="demo-simple-select-label">Category</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label={"Category"}
              value={category}
              onChange={handleChange}
            >
              {categories?.map(({ category_id, name }) => (
                <MenuItem value={name} key={category_id}>
                  {name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>

      
       
      </Paper>
    </Box>
  );
};

export default  EditCategory ;
