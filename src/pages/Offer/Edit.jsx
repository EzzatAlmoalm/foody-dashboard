import styled from "@emotion/styled";
import { useParams } from "react-router-dom";
import {
  Autocomplete,
  Box,
  Button,
  Chip,
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import React, { useRef, useState } from "react";
import { categories } from "../../data/categories";
import { products } from "../../data/products";
import { Offers } from "../../data/Offer";

const Editoffer = () => {
    const { id } = useParams();
    const selectedOffer = Offers.find((offer) => offer.id === Number(id));

  return (
    
   
    
    <Box sx={{ pt: "80px", pb: "20px" }}>
      <Typography variant="h6" sx={{ marginBottom: "14px" }}>
        Add Offer
      </Typography>
      <Paper
        sx={{
          boxShadow: "none !important",
          borderRadius: "12px",
          borderStyle: "solid",
          borderWidth: "1px",
          borderColor: "divider",
          p: "20px",
          maxWidth: "800px",
          margin: "0 auto",
          cursor: "pointer",
          overflow: "hidden",
        }}
      >
        <Box sx={{ mt: 4, display: "flex", alignItems: "center", gap: 4 }}>
          <FormControl fullWidth size="small">
            <InputLabel id="demo-simple-select-label">{selectedOffer?.category || ""}</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Category"
            >
              {Offers?.map(({ id, product_name }) => (
                <MenuItem value={product_name} key={id}>
                  {product_name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>

          <FormControl fullWidth size="small">
            <InputLabel id="demo-simple-select-label">{selectedOffer?.product_name || ""}</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Category"
            >
              {categories?.map(({ category_id, name }) => (
                <MenuItem value={name} key={category_id}>
                  {name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>


        <Box sx={{ mt: 4, display: "flex", alignItems: "center", gap: 4 }}>
          <TextField
            label= {selectedOffer?.discount_value || ""}
            variant="outlined"
            rows={4}
            fullWidth
            size="small"
          />
          <TextField
            label={selectedOffer?.discount_type || "Empty"}
            variant="outlined"
            rows={4}
            fullWidth
            size="small"
          />
        </Box>
         <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            mt: "30px",
          }}
        >
          <Button variant="contained" sx={{ borderRadius: "20px" }}>
            Submit
          </Button>
        </Box>
      </Paper>
    </Box>
  );
};

export default Editoffer;
