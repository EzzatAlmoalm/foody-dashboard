import { Box, Button, Typography } from "@mui/material";
import React from "react";
import { FiPlus } from "react-icons/fi";
import Table from "../../components/Table";
import { Offers, OfferColumns } from "../../data/Offer";

const Offer = () => {

  return (
    <Box sx={{ pt: "80px", pb: "20px" }}>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Typography variant="h6">Offer</Typography>

        <Button
          href="/Offer/add"         
          variant="contained"
          color="primary"
          startIcon={<FiPlus />}
          sx={{ borderRadius: "20px" }}
        >
          Add Offer
        </Button>
      </Box>
      <Table
        data={Offers}
        fields={OfferColumns}
        numberOfRows={Offers.length}
        enableTopToolBar={true}
        enableBottomToolBar={true}
        enablePagination={true}
        enableRowSelection={true}
        enableColumnFilters={true}
        enableEditing={true}
        enableColumnDragging={true}
        routeLink="offer"
      />
    </Box>
  );
};

export default Offer;
